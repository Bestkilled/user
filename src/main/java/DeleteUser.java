import java.sql.*;

public class DeleteUser {
     public static void main(String args[]) {

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:User.db");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "DELETE from USER where ID=2;";
            stmt.executeUpdate(sql);
            c.commit();

            ResultSet rs = stmt.executeQuery("SELECT * FROM USER;");

            while (rs.next()) {
                 int id = rs.getInt("ID");
                String name = rs.getString("USERNAME");
                String pass = rs.getString("PASSWORD");

                System.out.println("ID = " + id);
                System.out.println("NAME = " + name);
                System.out.println("PASSWORD = " + pass);
                System.out.println();
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }
}
